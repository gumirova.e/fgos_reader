import UI.Window
import DocxParser
import ExcelService
import TableModels
import sys
import os
from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QMainWindow, QApplication, QInputDialog
from PyQt6.QtGui import QAction, QIcon
from PyQt6.QtCore import QSize, QThread, pyqtSignal
import openpyxl
import sqlite3

TABLE_SHEETS = ["Структура и объем программы", "Компетенции", "Проф стандарты"]
DB_PATH = "data.db"


class DocxParserThread(QThread):
    finished = pyqtSignal(str, object, object)
    error = pyqtSignal(str)

    def __init__(self, docx_dir):
        super().__init__()
        self.docx_dir = docx_dir

    def run(self):
        try:
            docx_info = DocxParser.DocxParser(self.docx_dir)
            text_data = docx_info.parse_text()
            table_data = docx_info.parse_tables()
            title = docx_info.get_title()
            self.finished.emit(title, text_data, table_data)
        except Exception as e:
            self.error.emit(str(e))


class App(QMainWindow, UI.Window.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.docx_dir = ''
        self.setupUi(self)
        self.initUI()
        self.init_db()

    def initUI(self):
        self.setFixedSize(QSize(1200, 800))
        self.setWindowTitle("ФГОС")

        button_open_docx = QAction(QIcon("image/folder-open-image.png"), "Открыть...", self)
        button_create_excel = QAction(QIcon("image/blue-document-excel-table.png"), "Сохранить как Excel", self)
        button_load_db = QAction(QIcon("image/database-import.png"), "Загрузить из БД", self)
        menu = self.menuBar()

        file_menu = menu.addMenu("&Файл")
        file_menu.addAction(button_open_docx)
        file_menu.addAction(button_create_excel)
        file_menu.addAction(button_load_db)
        file_menu.addSeparator()

        button_open_docx.triggered.connect(self.open_docx)
        button_create_excel.triggered.connect(self.save_as_excel)
        button_load_db.triggered.connect(self.load_from_db)

    def init_db(self):
        conn = sqlite3.connect(DB_PATH)
        cursor = conn.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS docx_data (
                            id INTEGER PRIMARY KEY AUTOINCREMENT,
                            title TEXT,
                            general_info TEXT,
                            mandatory_disciplines TEXT,
                            structure_volume_program TEXT,
                            competencies TEXT,
                            prof_standards TEXT)''')
        conn.commit()
        conn.close()

    def save_to_db(self, title, text_data, table_data):
        conn = sqlite3.connect(DB_PATH)
        cursor = conn.cursor()

        cursor.execute('SELECT id FROM docx_data WHERE title = ?', (title,))
        if cursor.fetchone() is None:
            general_info = str(text_data[0])
            mandatory_disciplines = str(text_data[1])
            structure_volume_program = str(table_data[0])
            competencies = str(table_data[1])
            prof_standards = str(table_data[2])

            cursor.execute('''INSERT INTO docx_data (title, general_info, mandatory_disciplines, structure_volume_program, competencies, prof_standards) 
                                 VALUES (?, ?, ?, ?, ?, ?)''',
                           (title, general_info, mandatory_disciplines, structure_volume_program, competencies,
                            prof_standards))
            conn.commit()
        conn.close()

    def load_from_db(self):
        conn = sqlite3.connect(DB_PATH)
        cursor = conn.cursor()
        cursor.execute('''SELECT title FROM docx_data''')
        titles = [row[0] for row in cursor.fetchall()]
        conn.close()

        if titles:
            title, ok = QInputDialog.getItem(self, "Выбор документа", "Выберите название документа:", titles, 0, False)
            if ok and title:
                self.load_data_by_title(title)

    def load_data_by_title(self, title):
        conn = sqlite3.connect(DB_PATH)
        cursor = conn.cursor()
        cursor.execute('''SELECT title, general_info, mandatory_disciplines, structure_volume_program, competencies, prof_standards 
                          FROM docx_data WHERE title = ?''', (title,))
        data = cursor.fetchone()
        conn.close()

        if data:
            title, general_info, mandatory_disciplines, structure_volume_program, competencies, prof_standards = data

            general_info = eval(general_info)
            mandatory_disciplines = eval(mandatory_disciplines)
            structure_volume_program = eval(structure_volume_program)
            competencies = eval(competencies)
            prof_standards = eval(prof_standards)

            text_data = (general_info, mandatory_disciplines)
            table_data = (structure_volume_program, competencies, prof_standards)

            self.setWindowTitle(title)  # Set window title
            self.fill_tables(text_data, table_data)

    def open_docx(self):
        directory = QtWidgets.QFileDialog.getOpenFileName(self, "Выберите файл docx",
                                                          os.path.abspath(os.path.join(__file__, "../..")),
                                                          "Документ (*.docx)")[0]
        if directory != '':
            self.docx_dir = directory
            self.start_parsing_thread()

    def start_parsing_thread(self):
        self.thread = DocxParserThread(self.docx_dir)
        self.thread.finished.connect(self.on_parsing_finished)
        self.thread.error.connect(self.on_parsing_error)
        self.thread.start()

    def on_parsing_finished(self, title, text_data, table_data):
        self.setWindowTitle(title)
        self.fill_tables(text_data, table_data)
        self.save_to_db(title, text_data, table_data)

    def on_parsing_error(self, error_message):
        QtWidgets.QMessageBox.critical(self, "Ошибка", f"Не удалось обработать файл: {error_message}")

    def fill_tables(self, text_data, table_data):
        # заполнение таблиц окна
        general_info = text_data[0]
        max_amount_value = len(general_info[-1])
        for i, arr in enumerate(general_info):
            general_info[i].extend([''] * (max_amount_value - len(arr)))
        model_1 = TableModels.TableReversedModel(general_info)
        self.tableView_1.setModel(model_1)
        self.set_style_tableView(self.tableView_1, len(general_info))

        model_2 = TableModels.TableReversedModel(text_data[1])
        self.tableView_2.setModel(model_2)
        self.set_style_tableView(self.tableView_2)

        model_3 = TableModels.TableModel(table_data[0])
        self.tableView_3.setModel(model_3)
        self.tableView_3.resizeColumnsToContents()

        model_4 = TableModels.TableModel(table_data[1])
        self.tableView_4.setModel(model_4)
        self.set_style_tableView(self.tableView_4, 2)

        model_5 = TableModels.TableModel(table_data[2])
        self.tableView_5.setModel(model_5)
        self.set_style_tableView(self.tableView_5)

    def set_style_tableView(self, tableView, column_number=1):
        column_size = 1110 // column_number
        for i in range(column_number):
            tableView.setColumnWidth(i, column_size)
        tableView.resizeRowsToContents()

    def save_as_excel(self):
        excel_name = self.windowTitle()
        excel_path = [os.path.dirname(os.path.abspath(self.docx_dir)), f'{excel_name}.xlsx']
        dir_excel_current = QtWidgets.QFileDialog.getSaveFileName(self,
                                                                  "Сохранить файл",
                                                                  os.sep.join(excel_path),
                                                                  "Excel File (*.xlsx)")
        if dir_excel_current[0] != '':
            wb = openpyxl.Workbook()
            excel = ExcelService.ExcelFill(wb)
            data = self.get_docx_data(True)
            excel.fill_text(data[0], "Общая информация")
            excel.fill_text(data[1], "Обязательные дисциплины")
            excel.fill_table(data[2], TABLE_SHEETS)

            del wb['Sheet']
            wb.save(dir_excel_current[0])

    def get_docx_data(self, is_for_excel=False):
        docx_info = DocxParser.DocxParser(self.docx_dir)
        text_data = docx_info.parse_text()
        table_data = docx_info.parse_tables()
        if not is_for_excel:
            title = docx_info.get_title()
            self.setWindowTitle(title)
            self.fill_tables(text_data, table_data)
            self.save_to_db(title, text_data, table_data)  # Save data to the database
        else:
            return text_data + tuple(table_data)


def main():
    app = QApplication(sys.argv)
    ui = App()
    ui.show()
    app.exec()


if __name__ == '__main__':
    main()
