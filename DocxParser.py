import docx
from docx.oxml.table import CT_Tbl
from docx.oxml.text.paragraph import CT_P
from docx.text.paragraph import Paragraph
import re

NUMB_SYMB = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']


class DocxParser:
    def __init__(self, dir_docx):
        self.doc = docx.Document(dir_docx)

    def get_title(self):
        """Формирование заголовка для app"""
        date = ''
        for paragraph in self.doc.paragraphs:
            if date == '':
                if not re.search(r"от \d{1,2}", paragraph.text):
                    continue
                else:
                    date = paragraph.text
            spec = re.search(r"\d{2}.\d{2}.\d{2}", paragraph.text)
            if spec:
                title = spec[0] + ' ' + date
                return title

    def parse_text(self):
        """Парсинг нужных пунктов текста"""
        key_text = [r"В рамках освоения программы",
                    r"При разработке программы \w{1,} Организация \w{1,}",
                    r"Программа \w{1,} должна обеспечивать реализацию дисциплин",
                    r"Программа \w{1,} должна обеспечивать реализацию дисциплин",
                    r"В Блок 2 \"Практика\" входят учебная и производственная практики",
                    r"Программа \w{1,} должна устанавливать следующие общепрофессиональные компетенции:", "df"]

        pars_text = []
        flag = 0
        count = 0
        for paragraph in self.doc.paragraphs:
            text = paragraph.text
            if text != '':
                if (paragraph.style.name == 'List Paragraph' or text[0] in NUMB_SYMB) and re.search(key_text[count],
                                                                                                    text):
                    pars_text.append([text])
                    flag = 1
                    count += 1
                elif flag == 1 and text[0] not in NUMB_SYMB and paragraph.style.name != 'List Paragraph':
                    if text != '':
                        pars_text[count - 1].append(text)
                else:
                    flag = 0

        return self.reformat_text(pars_text)

    def reformat_text(self, pars_text):
        """Форматирование текста к нужному виду"""
        # формирование списка обязательных дисциплин (текст->таблица)
        required_disp = pars_text.pop(2)
        if required_disp[0].count(','):
            text_to_table = required_disp[0].split(', ')
        else:
            text_to_table = required_disp[0].split('; ')
        text_to_table[0] = text_to_table[0][-9:]
        text_to_table[-1] = text_to_table[-1][:-41]

        # обозначение заголовка обязательных дисциплин
        k = 77
        for i, s in enumerate(required_disp[0]):
            if s == ':':
                k = i
                break
        if required_disp[0][0] in NUMB_SYMB:
            title_disciplines = [required_disp[0][5:k + 5]]
        else:
            title_disciplines = [required_disp[0][:k]]
        text_to_table = title_disciplines + text_to_table + [''] + pars_text.pop(2)

        cloned_pars = []
        general_info = []
        for i, block in enumerate(pars_text):
            cloned_pars.append([])
            for j, line in enumerate(block):
                cloned_pars[i].extend(line.split(': '))

        for i, block in enumerate(cloned_pars):
            general_info.append([])
            for j, line in enumerate(block):
                general_info[i].extend(line.split('; '))

        # удаление типов из практик
        for y, l in enumerate(general_info[-2]):
            if re.search(r"Типы", l):
                general_info[-2].pop(y)

        # сортировка опк
        x = []  # массив кортежей номер опк и строка
        for opk in general_info[-1]:
            run = 0
            line = ''
            for symb in opk[3:]:
                if symb in NUMB_SYMB:
                    run = 1
                    line += symb
                elif run == 1 and line[:-1] != '':
                    if line[-1] == '.':
                        x.append(tuple([float(line[:-1]), opk]))
                    else:
                        x.append(tuple([float(line), opk]))
                    break

        s = sorted(x, key=lambda tup: tup[0])
        general_info[-1][1:] = [x[1] for x in s]

        return general_info, [text_to_table]

    def iter_block_items(self):
        """Объединение разделенных страницами таблиц"""
        doc_elm = self.doc.element.body
        map_split_table = []
        is_table = False
        count = 0

        for child in doc_elm.iterchildren():
            if is_table and isinstance(child, CT_P):
                if Paragraph(child, self.doc).text == '':
                    continue
                else:
                    is_table = False
                    count += 1
            elif isinstance(child, CT_Tbl):
                is_table = True
                map_split_table.append(count)

        return map_split_table

    def parse_tables(self):
        """Считывание содержимого таблиц"""
        map_split_table = self.iter_block_items()
        tables_data = [[], [], [], []]
        for iTable, table in enumerate(self.doc.tables):
            current_table = tables_data[map_split_table[iTable]]
            for iRow, row in enumerate(table.rows):
                current_table.append([])
                for cell in row.cells:
                    current_table[len(current_table) - 1].append(cell.text)

        # Взятие столбца Код профессионального стандарта из последней таблицы
        num_col = 1
        for i, column_names in enumerate(tables_data[-2][0]):
            if re.search(r"Код", column_names):
                num_col = i
                break

        for j in tables_data[-2]:
            if j[num_col] != '':
                tables_data[-1].append([j[num_col]])
        tables_data.pop(-2)

        return tables_data

