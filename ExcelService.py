from openpyxl.styles import Border, Side, Alignment


class ExcelFill:
    def __init__(self, wb):
        self.wb = wb

    def fill_text(self, list_text, sheet_name):
        sheet = self.wb.create_sheet(sheet_name)
        thin = Side(border_style="thin", color="000000")

        for iPar, par in enumerate(list_text):
            sheet.column_dimensions[chr(ord('@') + iPar + 1)].width = 50
            for iCol, col in enumerate(sheet.iter_cols(min_col=iPar + 1, max_col=iPar + 1, max_row=len(par))):
                for iCell, cell in enumerate(col):
                    cell.value = par[iCell]
                    cell.alignment = Alignment(wrap_text=True, vertical='top')
                    cell.border = Border(top=thin, left=thin, right=thin, bottom=thin)

    def fill_table(self, table_data, table_sheets):
        dc = dict(zip(table_sheets, table_data))
        thin = Side(border_style="thin", color="000000")
        for key in dc:
            sheet = self.wb.create_sheet(key)
            for iRow, row in enumerate(sheet.iter_rows(min_row=1, max_col=len(dc[key][0]), max_row=len(dc[key]))):
                for iCell, cell in enumerate(row):
                    cell.value = dc[key][iRow][iCell]
                    cell.border = Border(top=thin, left=thin, right=thin, bottom=thin)
            for column in sheet.columns:
                max_length = 0
                column_name = column[0].column_letter
                for cell in column:
                    try:
                        if len(str(cell.value)) > max_length:
                            max_length = len(str(cell.value))
                    except:
                        pass
                adjusted_width = (max_length + 2) * 1.1
                sheet.column_dimensions[column_name].width = adjusted_width

