import os


def auto_ui_to_py():
	for file in os.listdir():
		if file[-3:] == '.ui':
			print(file)
			os.system(f"python -m PyQt6.uic.pyuic -x {file} -o {file[:-3]}.py")
		elif '.qrc' in file:
			print(file)
			os.system(f"pyrcc6 -o {file.replace('.qrc', '.py')} {file}")


if __name__ == '__main__':
	auto_ui_to_py()
